# Windows Desktop Automation for Digital Bridge

This repro contains all tests and examples to validate if windows desktop automation for legacy Windows government software solutions is working.


## Overview of Available and Tested Tools

The following tools allow to automate desktop application under Windows:

Tool | Open Source | Summary
:--- | :---------: | :------
[AutoIt](https://www.autoitscript.com/site/autoit/) | no <br/> (but free) | **looks promising** - pure scripting - similar to AutoHotkey
[AutoHotKey]() | yes | **looks promising** - pure scripting - similar to AutoIt
[UiPath]() | no | heavy weight clicking tool that needs further investigation
[taskt](http://www.taskt.net/) | yes | not usable
[WindowsApplicationDriver]() | yes | **to be tested**
[pywinauto](https://pywinauto.github.io/) | yes | might be usable – needs further investigation
[RobotJS](http://robotjs.io/) | yes | not usable - very limited


## Test Cases

### Test Case 1 "Notepad++ printing paper orientation"

- Start or use the running Notepadd++ app
- Open printing dialog via main menu
- Open pref dialog via button click
- Set paper orientation to "Landscape" via combobox list item selection


## Test Results


### UiPath

**Positive**

* extensive docs and videos
* UI to create automated workflows
 
**Negative**

* very complex structure, e.g. results in many nested activities
* Re-running workflows resulted in different results, e.g. Notepad++ was often not started or the dialog did not open
* Test case could not be realized: orientation combobox could not be automated


### AutoIt

**Positive**

* simple script automation
* extensive Windows help file
* Test case was realized fairly quickly with a little work-around for the main menu which is not recognized as a control and can thus only be clicked via mouse coordinates :(
* Versioning and distributed coding via git possible

**Negative**

* Basic code
* No UI to create automated workflows > requires scripting experience

**To Be Evaluated**

* using the app to manage automation on many client PCs needs to be investigated
* Useability of large scripts
* availability of skilled students/developers


### AutoHotkey

**Positive**

* simple script automation
* extensive docs and examples
* Test case was realized fairly quickly with a little work-around for the orientation combobox
* Versioning and distributed coding via git possible

**Negative**

* few video tutorials
* strange syntax
* No UI to create automated workflows > requires scripting experience
* also unstable execution results: sometimes the script stops without going all through – but more often than UiPath > might need some tweaking of timing/waiting…

**To Be Evaluated**

* using the app to manage automation on many client PCs needs to be investigated
* Useability of large scripts
* availability of skilled students/developers


### taskt

**Positive**

* very simple and easy

**Negative**

* screen capture via mouse positioning and clicks only
* creates endless mouse click and window activate events
* could not execute a simple script > smallest recorded scripts result in an error :(
* very slow tool


### WindowsApplicationDriver

tbd


### pywinauto

**Positive**

* extensive well-written docs and tutorials (https://pywinauto.readthedocs.io/en/latest/index.html#what-is-it)
* proper python object-oriented automation programming

**Negative**

* steep learning curve - difficult to get started
* small OS community with rather few pull requests and commits - mainly bug fixes and smaller features


### RobotJS

**Positive**

* easy-to-use and start Javascript automation scripts
* integration of Javascript modules possible, e.g. for xml or starting applications

**Negative**

* automation via mouse and keyboard control only - no UI control selection

